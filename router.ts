import { body, query, param } from "express-validator";
import { Router } from "express";
import UserController from "./controller";

const router = Router();

router.delete('/users/:id', param('id').notEmpty().isUUID(), UserController.deleteUser);
router.get('/users/:id', param('id').notEmpty().isUUID(), UserController.getUserById);
router.post('/users', body('role').notEmpty(), UserController.addUser);
router.patch('/users/:id', body('role').notEmpty(), param('id').notEmpty(), UserController.updateUser);
router.get('/users',
    query('from').optional().isISO8601(),
    query('to').optional().isISO8601(),
    query('role').optional().notEmpty(),
    UserController.getAllUsers);
export default router;