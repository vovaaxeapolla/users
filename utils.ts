export function formatDate(date: Date | string) {
    if (typeof date === 'string')
        date = new Date(date);
    return `${date.getFullYear()}-${date.getMonth()}`;
}

export function getDatesInRange(from?: string, to?: string): Date[] {
    const start = from ? new Date(from) : new Date('2023-11-07');
    const end = to ? new Date(to) : new Date();
    const date = new Date(start.getTime());
    const dates: Date[] = [];
    while (date <= end) {
        dates.push(new Date(date));
        date.setMonth(date.getMonth() + 1);
    }
    return dates;
}