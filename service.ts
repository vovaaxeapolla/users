import client from './db';
import { formatDate } from './utils';
import { types } from 'cassandra-driver';

export default class UserService {

    static async getAllUsers() {
        const query = `SELECT * FROM users.users;`;
        const users = (await client.execute(query)).rows;
        return users;
    }

    static async getUserById(id: types.TimeUuid) {
        return (await client.execute(`SELECT id, dateof(id) AS created_at, role FROM users.users WHERE id = ?`, [id], { prepare: true })).rows[0];
    }

    static async getUsersByRole(role: string) {
        const query = `SELECT user_id FROM users.users_by_role WHERE role = ?`;
        const users = (await client.execute(query, [role])).rows;
        return users;
    }

    static async deleteUser(id: types.TimeUuid, role: string, created_at: string) {
        const delFromUsers = `DELETE FROM users.users WHERE id = ?`;
        const delFromUsersByRole = `DELETE FROM users.users_by_role WHERE role = ?  AND user_id = ?`;
        const delFromUsersByCreatedAt = `DELETE FROM users.users_by_created_at WHERE created_at = ?  AND user_id = ?`;
        client.execute(delFromUsers, [id], { prepare: true });
        client.execute(delFromUsersByRole, [role, id], { prepare: true });
        client.execute(delFromUsersByCreatedAt, [formatDate(created_at), id], { prepare: true });
    }

    static async addUser(id: types.TimeUuid, role: string, created_at: string | Date) {
        let query = `INSERT INTO users.users (id, role) VALUES(?, ?);`;
        let values = [id, role];
        client.execute(query, values, { prepare: true });

        const formattedDate = formatDate(created_at);

        query = `INSERT INTO users.users_by_created_at (user_id, created_at) VALUES(?, ?);`;
        values = [id, formattedDate];
        client.execute(query, values, { prepare: true });

        query = `INSERT INTO users.users_by_role (user_id, role) VALUES(?, ?);`;
        values = [id, role];
        client.execute(query, values, { prepare: true });
    }

    static async updateUser(id: types.TimeUuid, role: string, currentRole: string) {
        let query = `UPDATE users.users SET role = ? WHERE id = ?`;
        let values = [role, id];
        client.execute(query, values, { prepare: true });

        query = `DELETE FROM users.users_by_role WHERE role = ?  AND user_id = ?`;
        values = [currentRole, id];
        client.execute(query, values, { prepare: true });

        query = `INSERT INTO users.users_by_role (user_id, role) VALUES(?, ?);`;
        values = [id, role];
        client.execute(query, values, { prepare: true });
    }
}