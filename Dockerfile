FROM node 
WORKDIR /users

COPY package.json /users

RUN npm install

COPY . .

EXPOSE 80

CMD ["node", "index.js"]