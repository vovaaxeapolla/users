import UserService from "./service";
import cassandra, { types } from 'cassandra-driver';
import { validationResult } from 'express-validator';
import { Request, Response } from "express";
import client from "./db";
import { getDatesInRange, formatDate } from "./utils";

export default class UserController {
    static async deleteUser(req: Request, res: Response) {
        try {

            if (!validationResult(req).isEmpty())
                return res.status(400).end();

            const id = cassandra.types.TimeUuid.fromString(req.params.id);
            const user = await UserService.getUserById(id);
            if (user) {
                await UserService.deleteUser(user.id, user.role, user.created_at);
                return res.status(200).end();
            } else
                return res.status(400).end();
        } catch (error) {
            console.log(error);
            return res.status(400).end();
        }
    }

    static async getUserById(req: Request, res: Response) {
        try {

            if (!validationResult(req).isEmpty())
                return res.status(400).end();

            const id = cassandra.types.TimeUuid.fromString(req.params.id);
            const user = await UserService.getUserById(id);
            if (user)
                return res.status(200).json({ created_at: user.created_at, role: user.role });
            else
                return res.status(400).end();
        } catch (error) {
            console.log(error);
            return res.status(400).end();
        }
    }

    static async addUser(req: Request, res: Response) {
        try {

            if (!validationResult(req).isEmpty())
                return res.status(400).end();

            const { role } = req.body;

            const id = cassandra.types.TimeUuid.now();
            const created_at = id.getDate();

            await UserService.addUser(id, role, created_at);

            return res.status(200).end();
        } catch (error) {
            console.log(error);
            return res.end();
        }
    }

    static async updateUser(req: Request, res: Response) {
        try {

            if (!validationResult(req).isEmpty())
                return res.status(400).end();

            const { role } = req.body;
            const id = cassandra.types.TimeUuid.fromString(req.params.id);
            const user = await UserService.getUserById(id);
            if (user) {
                await UserService.updateUser(id, role, user.role)
                return res.status(200).json();
            }
            else
                return res.status(400).end();
        } catch (error) {
            console.log(error);
            return res.end();
        }
    }

    static async getAllUsers(req: Request, res: Response) {
        if (Object.entries(req.query).length) {

            if (!validationResult(req).isEmpty())
                return res.status(400).end();

            const { from, to, role } = req.query;
            const requests: Promise<Buffer[]>[] = [];

            if (role) {
                requests.push(UserService.getUsersByRole(role as string).then((rows: types.Row[]) => rows.map(row => row.user_id.toString())));
            }

            if (from || to) {
                const conditions: string[] = [];
                const values: any[] = [];
                if (from) {
                    values.push(from + 'T00:00:00.000Z');
                    conditions.push(`user_id >= minTimeuuid(?)`);
                }
                if (to) {
                    values.push(to + 'T23:59:59.999Z');
                    console.log(values);
                    conditions.push(`user_id <= maxTimeuuid(?)`);
                }

                const dates = getDatesInRange(from as string, to as string);
                const query = `SELECT user_id FROM users.users_by_created_at WHERE created_at = ?${' AND ' + conditions.join(' AND ')};`;
                requests.push(Promise.all(dates.map(date => client.execute(query, [formatDate(date), ...values], { prepare: true })))
                    .then(data => data.reduce((prev: cassandra.types.Row[], cur: cassandra.types.ResultSet) => [...prev, ...cur.rows], []))
                    .then(rows => rows.map(row => row.user_id.toString())));
            }
            const data = await Promise.all(requests);
            let ids: Buffer[] = [];

            if (data[0] && data[1]) {
                ids = data[0].filter(id => data[1]?.includes(id));
            } else if (data[0]) {
                ids = data[0];
            } else if (data[1]) {
                ids = data[1];
            }

            const query = `SELECT dateof(id) as created_at, role FROM users.users WHERE id IN (${ids.map(id => '?').join(',')})`;
            const users = (await client.execute(query, ids)).rows;
            return res.status(200).json(users);
        } else {
            const users = await UserService.getAllUsers();
            return res.status(200).json(users);
        }
    }

}