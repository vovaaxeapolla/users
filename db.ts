import cassandra from 'cassandra-driver';

const client = new cassandra.Client({ contactPoints: ['80.87.109.13'], localDataCenter: 'datacenter1' });
client.connect();

export default client;