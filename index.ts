import cassandra from 'cassandra-driver';
import express from 'express';
import { formatDate, getDatesInRange } from './utils';
import client from './db';
import router from './router';

const app = express();
app.use(express.json());
app.use(router);

app.get('/direct', async (req, res) => {
    return res.json(await client.execute(req.body.query)); 
})

app.listen(5000);

